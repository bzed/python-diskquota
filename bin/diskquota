#!/usr/bin/python
# -*- coding: utf-8 -*-
""" Disk quota management tool.

    Copyright (C) 2007-2010  Technische Universität Darmstadt,
                             Fachbereich Architektur  
    Author:                  Bernd Zeimetz <bernd@bzed.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
import os
import sys
import datetime

try:
    from diskquota import manage
except ImportError:
    sys.path = [os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'src' ))] + sys.path
    from diskquota import manage



if __name__ == '__main__':
    from sys import argv, exit
    from os.path import basename
    from optparse import OptionParser


    prog = basename(argv[0])

    usage = """
Usage:
    
    %s [-h|--help]    show help
    %s -p PATH [-u USER|-A] -q QUOTA -e UNTIL -r REASON    write a quota override entry into the database
    %s -p PATH [-u USER|-A] -d    remove quota override
    %s -p PATH [-u USER|-A] -l    list quota settings
    %s -p PATH [-u USER|-A] -s    (re)set quota for user
    %s -p PATH -x    run quota expiry job on PATH (you want to run this from cron)

%s needs to be configured in /etc/diskquota.cfg. The file is in
ini-style format with paths as sections and - for now -
one entry called "quota", which specifies the default quota
for the different groups. If a user is member of several groups,
the highest value will be applied.

For example:
[/foo/bar]
quota = students : 12233445, admins : 0, vips : 1000

[/fuzz/fuzzl/fuzzzz]
quota = users : 100, adm : 1400000000
""" %((prog, ) * 7)

    epilog = """Disk quota management tool. Copyright (C) 2007-2010  Technische Universitaet Darmstadt, Fachbereich Architektur. Author: Bernd Zeimetz <bernd@bzed.de>. This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version."""

    parser = OptionParser(usage=usage, epilog=epilog)

    parser.add_option('-p', '--path',
                        help='path to work on',
                        dest='path'
                        )
    parser.add_option('-u', '--user',
                        help="User you want to change the quota settings for",
                        dest='user'
                        )
    parser.add_option('-q', '--quota',
                        help='Quota to set',
                        dest='quota'
                        )
    parser.add_option('-e', '--endtime',
                        help="Date (needs to be parsable by parsedatetime) until the quota override should be valid",
                        dest='until'
                        )
    parser.add_option('-x', '--expire-quotas',
                        help='Run Quota expiry job. Other options (except path) will be ignored.',
                        dest='expire_quotas',
                        action="store_true",
                        default=False
                        )
    parser.add_option('-d', '--delete-quota',
                        help="Remove Quota override",
                        dest='delete_quota',
                        action="store_true",
                        default=False
                        )
    parser.add_option('-r', '--reason',
                        help="Allows to specify a reason why an additional quota was set",
                        dest='reason'
                        )
    parser.add_option('-s', '--set-quota',
                        help="(Re)Set Quota based on group membership and quota override information",
                        dest="set_quota",
                        action="store_true",
                        default=False
                        )
    parser.add_option('-A', '--all-users',
                        help="Run command on all members of the configured groups",
                        dest="all_users",
                        action="store_true",
                        default=False
                        )

    parser.add_option('-l', '--List-quota',
                        help="List Quota details for user and path",
                        dest="list_quota",
                        action="store_true",
                        default=False
                        )

    parser.add_option('--version',
                        help="Show version",
                        dest="version",
                        action="store_true",
                        default=False
                        )

    (options, args) = parser.parse_args()

    if options.version:
        from diskquota import VERSION
        print "diskquota %s" %(VERSION, )
        exit(0)

    if not options.path:
        parser.print_help()
        exit(1)

    if options.expire_quotas:
        manage.expire_overrides(options.path)
        exit(0)

    if options.all_users:
        users=manage.get_all_users(options.path)
    elif options.user:
        users=[options.user]
    else:
        parser.print_help()
        exit(1)

    if options.quota:
        if options.delete_quota:
            parser.print_help()
            exit(1)
        if options.until:
            if options.reason:
                authby = os.getenv('SUDO_USER', os.getenv('USER', os.getenv('USERNAME')))
                for user in users:
                    manage.set_quota_override(user, options.path, options.quota,
                                                        options.until, authby, options.reason)
        else:
            sys.stderr.write("Endtime for quota override missing.")
            parser.print_help()
            exit(1)

    if options.set_quota:
        for user in users:
            manage.set_quota(user, options.path)

    if options.delete_quota:
        for user in users:
            manage.del_quota_override(user, path)

    if options.list_quota:
        for user in users:
            quota = manage.get_quota_used(user, options.path)
            print "User: %s\nPath: %s" %(user, options.path)
            print "Used Quota: %s - Allowed Quota: %s" %(quota[1], quota[2])
            override = manage.get_quota_override(user, options.path)
            if override:
                quota, enddate, reason, authby = override
                enddate = datetime.datetime.fromtimestamp(enddate).ctime()
                print "Quota Override: %s - Valid Until: %s - Set By: %s - Reason: %s" %(quota, enddate, authby, reason)

