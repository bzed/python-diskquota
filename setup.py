#!/usr/bin/python
# -*- coding: utf-8 -*-
""" Copyright (C) 2007  Technische Universität Darmstadt,
                        Fachbereich Architektur  
                        Bernd Zeimetz <bernd@bzed.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

__long_desc__ = """ Very simple disk quota management module,
using Popen to run the OS' quota tools and parsing their output
should make it easy to modify/extend the module for other
Unix/Linux/BSD-like operating systems.

It comes with a tool to mange group based quota policies and
was developed for the specific needs of
Techniche Universität Darmstadt, Fachbereich Architektur
"""


from distutils.core import setup

import sys
sys.path.insert(0, 'src')
from diskquota import VERSION

classifiers = ['Development Status :: 3 - Alpha',
               'License :: OSI Approved :: GNU General Public License (GPL)',
               'Operating System :: POSIX :: Linux',
               'Programming Language :: Python',
               'Topic :: System :: Systems Administration']

setup(name="diskquota",
      version=VERSION,
      author="Bernd Zeimetz",
      author_email="bernd@bzed.de",
      license = "GPL v2 or later",
      platforms = ["GNU/Linux"],
      description = 'Disk quota management module',
      classifiers = classifiers,
      package_dir = {'': 'src'},
      packages = ['diskquota'],
      scripts=['bin/diskquota'],
      long_description = __long_desc__,
      data_files=[('/etc', [ 'src/diskquota/diskquota.cfg'])]
      )
