# -*- coding: utf-8 -*-
"""
    Copyright (C) 2007-2010  Technische Universität Darmstadt,
                             Fachbereich Architektur  
    Author:                  Bernd Zeimetz <bernd@bzed.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

__author__ = "Bernd Zeimetz"
__contact__ = "bernd@bzed.de"
__license__ = """GPL, either version 2 of the License,
or (at your option) any later version."""


from .quotadb import Quotadb
from . import config

from . import diskquota
import grp
import pwd
import time

__all__ = ['set_quota', 'get_quota_override', 'del_quota_override',
           'get_quota_used']

def __user_in_group__(user, gid):
    """Check if user is member of group."""
    if pwd.getpwnam(user)[3] == gid:
        return True
    if user in grp.getgrgid(gid)[3]:
        return True
    return False

def __find_configured_path__(path):
    """Find the configured path where a given path belongs to."""
    c_paths = config.get_paths()
    c_paths.sort()
    for c_path in reversed(c_paths):
        if path.startswith(c_path):
            return c_path
    return None

def __path_groups__(path):
    """Return the path we have to set quotas on
       and the configured groups for this path.
    """
    config_path = __find_configured_path__(path)
    if not config_path:
        raise ValueError("Could not find a valid configuration for %s" %(path, ))
    groups = config.get_quota_settings(config_path)
    return groups

def get_all_users(path):
    """Return the list of users which are member of the
       groups configured for path.
    """
    groups = __path_groups__(path)
    users = []
    for g in groups:
        users += grp.getgrgid(g).gr_mem
    usersset = {}
    map(usersset.__setitem__, users, [])
    return usersset.keys()

def set_quota(user, path):
    """Set a user's quota."""
    #set path down to the mountpoint it resides on

    groups = __path_groups__(path)

    new_quota = None
    override = get_quota_override(user, path)
    if override:
        quota, enddate = override[0:2]
        if not enddate < time.time():
            new_quota = quota
    #if there is no quota override:
    #check if user is member in a group we want to set quotas for.    
    if new_quota == None:
        for gid in groups.iterkeys():
            if __user_in_group__(user, gid):
                if groups[gid] == 0:
                    new_quota = 0
                elif (not new_quota == None) or groups[gid] > new_quota:
                    new_quota = groups[gid]
    
    #if we need to set a quota:
    if not new_quota == None:
        diskquota.setquota(user, new_quota, new_quota, 0, 0, False)

def get_quota_override(user, path):
    """Returns the value/reason/auth_by of the current quota override for a
       given path + username. Returns None if there is no override.
    """
    q_db = Quotadb(path)
    override = q_db.getquota(user)
    if not override:
        return None
    uid, quota, enddate, authby, reason = override
    
    return [quota, enddate, reason, authby]

def del_quota_override(user, path):
    """Removes a quota override for a user/path."""
    q_db = Quotadb(path)
    q_db.delquota(user)
    set_quota(user, path)

def get_quota_used(user, path):
    """Returns the Quota which is currently used by a user."""
    
    return diskquota.quota_by_path(path, user, False)

def set_quota_override(user, path, quota, enddate, authby, reason=""):
    """Set a quota override for user/path"""
    q_db = Quotadb(path)
    return  q_db.setquota(user, quota, enddate, authby, reason)

def expire_overrides(path):
    """Clean old entries from the DB"""
    q_db = Quotadb(path)
    for uid in q_db.expired_overrides():
        set_quota(uid[0], path)
    q_db.clean_db()
    return


