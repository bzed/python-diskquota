# -*- coding: utf-8 -*-
""" Very simple disk quota management module,
    using Popen to run the OS' quota tools and parsing their output
    should make it easy to modify/extend the module for other
    Unix/Linux/BSD-like operating systems.

    Copyright (C) 2007-2010  Technische Universität Darmstadt,
                             Fachbereich Architektur  
    Author:                  Bernd Zeimetz <bernd@bzed.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

__author__ = "Bernd Zeimetz"
__contact__ = "bernd@bzed.de"
__license__ = """GPL, either version 2 of the License,
or (at your option) any later version."""

__all__ = ['mount', 'quota', 'quota_by_device', 'quota_by_path', 'setquota_batch', 'setquota', 'find_mountpoint_by_path']

import os
import re
from subprocess import Popen, PIPE

QUOTA_BIN = "/usr/bin/quota"
SETQUOTA_BIN = "/usr/sbin/setquota"
MOUNT_BIN = "/bin/mount"

class QuotaException(Exception):
    """Exception to be raised if something went wrong
       during the handling of quotas
    """
    pass


def normalize_path(path):
    """Return the normalized and dereferenced path.
       
       @param path original pathname
       @type path string
       @return normalized and dereferenced path 
       @rtype string
    """ 
    
    path = os.path.realpath(os.path.normpath(path))
    return path



__mount_regex__ = re.compile(r"^(.*) on (.*) type (.*) \((.*)\)$", re.M)
def mount():
    """Return an array of mountpoints on the system
       
       @return sorted list of mountpoints [(device, mountpoint, type, options), ...]
       @rtype list
    """
       
    mount_p = os.popen(MOUNT_BIN)
    mount_list = mount_p.read()
    mount_close = mount_p.close()
    if mount_close:
        raise QuotaException('mount exited with ' + str(mount_close))
    
    return __mount_regex__.findall(mount_list)


def filter_mountpoint_list(mounts=None, devices = None, mountpoints = None,
                                types = None, options = None):
    """Return a filtered a mountpoint list,
       for example the output of mount().
       
       @param mounts list of mounts
       @type mounts list
       @param devices search for a device 
       @type devices list
       @param mountpoints search for a mountpoint
       @type mountpoints list
       @param types search for a fs type
       @type types list
       @param options search for mounts which were mounted with this option
       @type options list
       
       @return filtered list
       @rtype list
    """
    if not mounts:
        mounts = mount()

    out = []
    for line in mounts:
        addentry = True
        if devices:
            if not line[0] in devices:
                addentry = False
        if mountpoints:
            if not line[1] in mountpoints:
                addentry = False
        if types:
            if not line[2] in types:
                addentry = False
        if options:
            m_options = line[3].split(',')
            for option in options:
                if not option in m_options:
                    addentry = False
                    break    
        if addentry:
            out.append(line)
    
    return out

def find_mountpoint_by_path(path, mounts=None):
    """Return a mount tuple which provides path
    
       @param path path to search for
       @type path string
       @param mounts list of mounts, as provided by mount() (optional)
       @type mounts list
       
       @return mount tuple (device, mountpoint, type, options)
       @rtype tuple
    """
    if not mounts:
        mounts = mount()
    path = normalize_path(path)
    path_list = path.split('/')
    for subdir in reversed(range(2, len(path_list)+1)):
        subdir = '/'.join(path_list[0:subdir])
        if os.path.ismount(subdir):
            return filter_mountpoint_list(mounts, mountpoints=[subdir])[0]
    return None


__quota_regex__ = re.compile(
     r"^ *(/[^ \n]*) *\n? +([0-9]+) +([0-9]+) +([0-9]+) +([0-9]+) +([0-9]+) +([0-9]+) *$",
     re.M)
def quota(ugid, is_group=False):
    """Return a list of tuples listing the actual quota entries for 'id'.
       
       @param ugid user/groupname or uid/gid
       @param ugid string or int
       @param is_group=False set to True if you're looking for a group
       @param is_group bool
       
       @return list of quotas for ugid
       @rtype list
    """
    
    #do not use -w here as this option is not supported in solaris
    quota_cmd = [QUOTA_BIN, '-v']
    if is_group:
        quota_cmd.append('-g')
    else:
        quota_cmd.append('-u')
    quota_cmd.append(str(ugid))
    quota = Popen(quota_cmd, stdout=PIPE)
    quota_out = quota.communicate()[0]
    if quota.returncode >0:
        raise QuotaException('quota exited with ' + str(quota.returncode))
    
    return __quota_regex__.findall(quota_out)

def quota_by_device(device, ugid, is_group=False):
    """Return a user/group quota tuple for a given devicename.
       
       @param device name of a device which with quotas enabled
       @type device string
       @param ugid user/groupname or uid/gid
       @type ugid string or int
       @param is_group=False set to True if you're looking for a group
       @type is_group bool
       @rtype tuple or None
    """
    
    quota_list = quota(ugid, is_group)
    for dev in quota_list:
        if dev[0] == device:
            return dev
    return None

def quota_by_path(path, ugid, is_group=False):
    """Return a user/group quota tuple for a given path.
       
       @param path name of a path residing on a device with enabled quotas
       @type path string
       @param ugid user/groupname or uid/gid
       @type ugid string or int
       @param is_group=False set to True if you're looking for a group
       @type is_group bool
       @return quota tuple for a given path
       @rtype tuple
    """
    
    if is_group:
        mount_option = 'grpquota'
    else:
        mount_option = 'usrquota'
    
    mounts = filter_mountpoint_list(mount(),
                                        options=[mount_option])
    mountpoint = find_mountpoint_by_path(path, mounts)
    
    if not mountpoint:
        return None
    
    device = mountpoint[0]
    return quota_by_device(device, ugid, is_group)


def setquota_batch(quotalist, is_group=False, filesys=None):
    """Set quotas in batch mode.
       
       @param quotalist list of all quotas to set 
                        [(user/groupname, block-softlimit,
                        block-hardlimit, inode-softlimit,
                        inode-hardlimit), (...), ...]
       @param is_group set, if names in quotalist are group ids
       @param filesys: filesystem to set quotas on.
                       If omitted: set on all mounted filesystems  
       @raise QuotaException
       
       @todo add support for solaris & co....
    """
    quotastring = []
    for line in quotalist:
        quotastring.append(' '.join([str(x) for x in line]))

    # yes we need the additional \n at the end here, otherwise
    # setquota complains about a too long line...
    quotastring = '\n'.join(quotastring) + '\n'
    setquota_cmd = [SETQUOTA_BIN, "-b"]
    if is_group:
        setquota_cmd.append("-g")
    else:
        setquota_cmd.append("-u")
    if filesys:
        setquota_cmd.append(filesys)
    else:
        setquota_cmd.append("-a")
    
    if quotastring:
        setquota = Popen(setquota_cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        setquota.stdin.write(quotastring)
        stdoutdata, stderrdata = setquota.communicate()

        if stdoutdata or stderrdata:
            raise QuotaException("setquota: %s\n%s" %(stdoutdata, stderrdata))
    return

def setquota(ugid, bsoft, bhard, isoft, ihard,
              is_group=False, filesys=None):
    """Set quota for a single user/group.
       
       @param ugid user/groupname or uid/gid
       @type ugid string or int
       @param bsoft block-softlimit
       @type bsoft int
       @param bhard block-hardlimit
       @type bhard int
       @param isoft inode-softlimit
       @type isoft int
       @param ihard inode-hardlimit
       @type ihard int
       @param is_group set, if ugid is a group id
       @type is_group bool
       @param filesys device or mountoint to apply quota on.
                      if not set, the same quota is applied on all devices
       @type filesys string
       
       @raise QuotaException
    """
    return setquota_batch([(ugid, bsoft, bhard, isoft, ihard)],
                           is_group, filesys)
    
