# -*- coding: utf-8 -*-
""" Config file handling for diskquotad

    Copyright (C) 2007-2010  Technische Universität Darmstadt,
                             Fachbereich Architektur  
    Author:                  Bernd Zeimetz <bernd@bzed.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

__author__ = "Bernd Zeimetz"
__contact__ = "bernd@bzed.de"
__license__ = """GPL, either version 2 of the License,
or (at your option) any later version."""


__all__ = ['get_quota_settings']

try:
    import SafeConfigParser as ConfigParser
except ImportError:
    import ConfigParser

from application.configuration import datatypes

import os

import grp
import re


class StringListDict(dict):
    """Returns a dict, entries are seperated by , as in datatypes.Stringlist,
       keys and values are seperated by :
    """
    _dict_re = re.compile(r'\s*:\s*')
    def __new__(cls, value):
        ret = {}
        for entry in datatypes.StringList(value):
            try:
                key, value = cls._dict_re.split(entry)
            except ValueError:
                raise ValueError, \
                      "%s must be in the form `key : value'." % entry
            
            ret[key] = value
        return ret

class NumValueStringListDict(StringListDict):
    """Similar to StringListDict, but it enforces to have numbers as values."""
    def __new__(cls, value):
        ret = StringListDict(value)
        for key, value in ret.iteritems():
            try:
                value = int(value)
            except ValueError:
                raise ValueError, "%s is not a number." % value
            
            ret[key] = value
        return ret

class GroupQuotaDict(NumValueStringListDict):
    """Ensures that keys in the dict are gids, even if they were given
       as names. It also checks if the groups exist.
    """
    def __new__ (cls, value):
        ret = NumValueStringListDict(value)
        for key, value in ret.iteritems():
            try:
                try:
                    numkey = int(key)
                except ValueError:
                    ret.pop(key)
                    key = grp.getgrnam(key)[2]
                    ret[key] = value
                else:
                    grp.getgrgid(numkey)
            except KeyError:
                raise ValueError, '%s is not a gid or groupname' % str(key)
        return ret
            


config = ConfigParser.ConfigParser()
config.read([os.path.join(os.path.realpath(os.path.dirname(__file__)), 'diskquota.cfg'),  '/etc/diskquota.cfg'])

def get_quota_settings(path):
    """ Return a dict with the quota settings for path."""
    if config.has_section(path):
        return GroupQuotaDict(config.get(path, 'quota'))
    else:
        raise ValueError, "no configuration for %s available" %(path,)

def get_paths():
    return config.sections()
