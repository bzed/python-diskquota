# -*- coding: utf-8 -*-
"""   
    Copyright (C) 2007  Technische Universität Darmstadt,
                        Fachbereich Architektur  
                        Bernd Zeimetz <bernd@bzed.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

__author__ = "Bernd Zeimetz"
__contact__ = "bernd@bzed.de"
__license__ = """GPL, either version 2 of the License,
or (at your option) any later version."""


try:
    from sqlite3 import dbapi2 as sqlite #IGNORE:F0401
except ImportError:
    from pysqlite2 import dbapi2 as sqlite

import diskquota
import os
import pwd
import sys
import time

import parsedatetime.parsedatetime as pdt
import parsedatetime.parsedatetime_consts as pdc
pdt._debug = False

DBNAME = 'quotad.db'
TABLE = 'quotad' 

LOCALE = os.getenv('LANG', 'de_DE.UTF-8')


class QuotadbException(Exception):
    """QuotadbException will be rised if something goes wrong while handling
       the quotda database."""
    
    def __init__ (self, errmsg):
        self.errmsg = errmsg
        Exception.__init__(self)
        
    def __str__(self):
        return self.errmsg


def fix_uid(uid):
    """Make sure we've got a numric uid, otherwise convert it.
       @param uid: UID
       @type uid: int | string
    """
    try:
        ret = int(uid)
    except ValueError:
        try:
            ret = pwd.getpwnam(uid)[2]
        except KeyError:
            raise QuotadbException('%s is not a known UID.' % uid)
    return ret


TABLE_CREATE = """
    CREATE TABLE %s (
        uid integer PRIMARY KEY,
        quota integer,
        enddate integer,
        authby varchar(255),
        reason text
    );
""" % TABLE

class Quotadb:
    """ Quotadb handles the initialation of the DB and provides the
        necessary get/set functions. Currently for user quotas only.
        Should be easy to add support for group quotas.
    """
           
    def __init__(self, mountpoint):
        """Set the db path, initialize the DB file if neccessary."""
        
        
        mountpoint = diskquota.normalize_path(mountpoint)
        if not mountpoint in [x[1] for x in diskquota.mount()]:
            raise QuotadbException('No filesystem mounted at %s.' % mountpoint)
        
        self.dbfile = os.path.sep.join([mountpoint, DBNAME])
        connection = self.__connect__()
        cursor = connection.cursor()
        cursor.execute('select name from sqlite_master where name=?', (TABLE, ))
        row = cursor.fetchone()
        if not row:
            cursor.execute(TABLE_CREATE)
            connection.commit()
        cursor.close()
        connection.close()


    def __connect__(self):
        """Wrapper around sqlite.connect to produce a better Exception.
           Connects to self.dbfile. """
        
        try:
            return sqlite.connect(self.dbfile)
        except:
            info = sys.exc_info()[0]
            raise QuotadbException("Connection to %s failed;\n%s" 
                                   %(self.dbfile, info))
 
    
    def setquota(self, uid, quota, enddate, authby, reason=""):
        """Insert/update a quota setting in the database."""
        
        uid = fix_uid(uid)
        try:
            enddate = int(enddate)
        except ValueError:
            p = pdt.Calendar(pdc.Constants(LOCALE))
            enddate = time.mktime(p.parse(enddate)[0])

        connection = self.__connect__()
        cursor = connection.cursor()
        cursor.execute('select * from %s where uid=?' %(TABLE, ), (uid, ))
        row = cursor.fetchone()
        if row:
            cursor.execute("""update %s set quota=?,
                                            enddate=?,
                                            authby=?,
                                            reason=? where uid=?"""
                                            %(TABLE, ),
                           (quota, enddate, authby, reason, uid, ))
        else:
            cursor.execute("""insert into %s 
                                 (uid, quota, enddate, authby, reason)
                                  values (?, ?, ?, ?, ?)""" % (TABLE, ),
                           (uid, quota, enddate, authby, reason, ))
        connection.commit()
        cursor.close()
        connection.close()
        
    def getquota(self, uid):
        """Return the database row for a specified user as dict."""

        uid = fix_uid(uid)
        connection = self.__connect__()
        cursor = connection.cursor()
        cursor.execute('select * from %s where uid=?' %(TABLE, ), (uid, ))
        row = cursor.fetchone()
        cursor.close()
        connection.close()
        return row

    def delquota(self, uid):
        """Deletes a quota entry for a uid."""
        
        uid = fix_uid(uid)
        connection = self.__connect__()
        cursor = connection.cursor()
        cursor.execute('delete from %s where uid=?' %(TABLE, ), (uid, ))
        connection.commit()
        cursor.close()
        connection.close()

    def expired_overrides(self):
        """ Returns a list of users with expired quota overrides. """
        connection = self.__connect__()
        cursor = connection.cursor()
        cursor.execute('select uid from %s where enddate < ?' %(TABLE, ),
                        (int(time.time()), ))
        ret = cursor.fetchall()
        cursor.close()
        connection.close()
        return ret

    def clean_db(self):
        """Removes all old entries from the DB."""
        connection = self.__connect__()
        cursor = connection.cursor()
        cursor.execute('delete from %s where enddate < ?' %(TABLE, ),
                        (int(time.time()), ))
        connection.commit()
        cursor.close()
        connection.close()
